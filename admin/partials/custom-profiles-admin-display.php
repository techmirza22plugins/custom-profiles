<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $tab = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : "instructions";
?>

<div class="wrap custom-profiles-settings-div">
    <h2><?php _e( 'Custom Profiles Settings', 'custom-profiles' ); ?></h2>
    <?php settings_errors(); ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=custom-profiles-settings&tab=instructions'); ?>" class="nav-tab <?php echo ($tab=='instructions') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Instructions', 'custom-profiles' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=custom-profiles-settings&tab=pages-settings'); ?>" class="nav-tab <?php echo ($tab=='pages-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Pages Settings', 'custom-profiles' ); ?>
        </a>
    </h2>
    <?php if ($tab == 'instructions') { ?>
        <ul>
            <li>Following is the list of shortcode for Custom Profiles which can be easily integrated into the pages.</li>
            <li>1. <code>[custom-profiles-register]</code> can be placed anywhere in the page to load <strong>Registration Page</strong>.</li>
            <li>2. <code>[custom-profiles-login]</code> can be placed anywhere in the page to load <strong>Login Page</strong>.</li>
            <li>3. <code>[custom-profiles-activate-kit]</code> can be placed anywhere in the page to load <strong>Activation Kit Page</strong>.</li>
            <li>4. <code>[custom-profiles-settings]</code> can be placed anywhere in the page to load <strong>Settings Page</strong>.</li>
            <li>5. <code>[custom-profiles-change-password]</code> can be placed anywhere in the page to load <strong>Change Password Page</strong>.</li>
            <li>6. <code>[custom-profiles-manage-users]</code> can be placed anywhere in the page to load <strong>Manage Users Page</strong>.</li>
            <li>7. <code>[custom-profiles-add-new-profile]</code> can be placed anywhere in the page to load <strong>Add New Profile Page</strong>.</li>
            <li>8. <code>[custom-profiles-edit-profile]</code> can be placed anywhere in the page to load <strong>Edit Profile Page</strong>.</li>
            <li>9. <code>[custom-profiles-forgot-password]</code> can be placed anywhere in the page to load <strong>Forgot Password Page</strong>.</li>
            <li>10. <code>[custom-profiles-reset-password]</code> can be placed anywhere in the page to load <strong>Reset Password Page</strong>.</li>
            <li>11. <code>[custom-profiles-barcode-dashboard]</code> can be placed anywhere in the page to load <strong>Barcode Dashboard Page</strong>.</li>
        </ul>
    <?php } ?>
    <?php if ($tab == 'pages-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'custom-profiles-settings' );
            do_settings_sections( 'custom-profiles-settings' );
            $customProfilesSettings = get_option('custom_profiles_settings', array());
            $custom_profiles_register_page_id = $customProfilesSettings['custom_profiles_register_page_id'];
            $custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
            $custom_profiles_activate_barcode_page_id = $customProfilesSettings['custom_profiles_activate_barcode_page_id'];
            $custom_profiles_settings_page_id = $customProfilesSettings['custom_profiles_settings_page_id'];
            $custom_profiles_change_password_page_id = $customProfilesSettings['custom_profiles_change_password_page_id'];
            $custom_profiles_manage_users_page_id = $customProfilesSettings['custom_profiles_manage_users_page_id'];
            $custom_profiles_support_page_id = $customProfilesSettings['custom_profiles_support_page_id'];
            $custom_profiles_terms_of_use_page_id = $customProfilesSettings['custom_profiles_terms_of_use_page_id'];
            $custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
            $custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
            $custom_profiles_forgot_password_page_id = $customProfilesSettings['custom_profiles_forgot_password_page_id'];
            $custom_profiles_reset_password_page_id = $customProfilesSettings['custom_profiles_reset_password_page_id'];
            $custom_profiles_barcode_dashboard_page_id = $customProfilesSettings['custom_profiles_barcode_dashboard_page_id']; ?>
            <table class="widefat form-table custom-profiles-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_register_page_id"><?php _e( 'Register Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_register_page_id]" id="custom_profiles_register_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_register_page_id) && !empty($custom_profiles_register_page_id) && $custom_profiles_register_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_login_page_id"><?php _e( 'Login Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_login_page_id]" id="custom_profiles_login_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_login_page_id) && !empty($custom_profiles_login_page_id) && $custom_profiles_login_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_activate_barcode_page_id"><?php _e( 'Activate Barcode Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_activate_barcode_page_id]" id="custom_profiles_activate_barcode_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_activate_barcode_page_id) && !empty($custom_profiles_activate_barcode_page_id) && $custom_profiles_activate_barcode_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_settings_page_id"><?php _e( 'Settings Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_settings_page_id]" id="custom_profiles_settings_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_settings_page_id) && !empty($custom_profiles_settings_page_id) && $custom_profiles_settings_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_change_password_page_id"><?php _e( 'Change Password Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_change_password_page_id]" id="custom_profiles_change_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_change_password_page_id) && !empty($custom_profiles_change_password_page_id) && $custom_profiles_change_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_manage_users_page_id"><?php _e( 'Manage Users Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_manage_users_page_id]" id="custom_profiles_manage_users_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_manage_users_page_id) && !empty($custom_profiles_manage_users_page_id) && $custom_profiles_manage_users_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_support_page_id"><?php _e( 'Support Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_support_page_id]" id="custom_profiles_support_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_support_page_id) && !empty($custom_profiles_support_page_id) && $custom_profiles_support_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_terms_of_use_page_id"><?php _e( 'Terms of Use Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_terms_of_use_page_id]" id="custom_profiles_terms_of_use_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_terms_of_use_page_id) && !empty($custom_profiles_terms_of_use_page_id) && $custom_profiles_terms_of_use_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_add_new_profile_page_id"><?php _e( 'Add New Profile Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_add_new_profile_page_id]" id="custom_profiles_add_new_profile_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_add_new_profile_page_id) && !empty($custom_profiles_add_new_profile_page_id) && $custom_profiles_add_new_profile_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_edit_profile_page_id"><?php _e( 'Edit Profile Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_edit_profile_page_id]" id="custom_profiles_edit_profile_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_edit_profile_page_id) && !empty($custom_profiles_edit_profile_page_id) && $custom_profiles_edit_profile_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_forgot_password_page_id"><?php _e( 'Forgot Password Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_forgot_password_page_id]" id="custom_profiles_forgot_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_forgot_password_page_id) && !empty($custom_profiles_forgot_password_page_id) && $custom_profiles_forgot_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_reset_password_page_id"><?php _e( 'Reset Password Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_reset_password_page_id]" id="custom_profiles_reset_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_reset_password_page_id) && !empty($custom_profiles_reset_password_page_id) && $custom_profiles_reset_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_profiles_barcode_dashboard_page_id"><?php _e( 'Barcode Dashboard Page', 'custom-profiles' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_profiles_settings[custom_profiles_barcode_dashboard_page_id]" id="custom_profiles_barcode_dashboard_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-profiles' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_profiles_barcode_dashboard_page_id) && !empty($custom_profiles_barcode_dashboard_page_id) && $custom_profiles_barcode_dashboard_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
</div>
