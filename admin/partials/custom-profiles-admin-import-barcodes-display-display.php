<?php
    $args = array(
        'post_type'=> 'custom-barcode',
        'orderby'    => 'ID',
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1 // this will retrive all the post that is published 
    );
    $barcodes = get_posts( $args );
?>
<div class="wrap custom-profiles-all-barcodes-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'All Barcodes', 'custom-profiles' ); ?></h2>
    <table id="example" class="display datatable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($barcodes) && is_array($barcodes) && count($barcodes) > 0) { ?>
                <?php $count = 0; foreach ($barcodes as $barcode) { ?>
                    <tr>
                        <td><?php echo ++$count; ?></td>
                        <td><?php echo $barcode->post_title; ?></td>
                        <td>
                            <?php
                                $used = get_post_meta( $barcode->ID, 'used', true );
                                if ($used) { ?>
                                    <span class="badge badge-success">Used</span>
                                <?php } else { ?>
                                    <span class="badge badge-danger">Not Used</span>
                                <?php }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>
</div>