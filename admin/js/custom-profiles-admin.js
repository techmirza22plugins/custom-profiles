(function( $ ) {

	var bar = '';
	var barInc = 0;
	var barCount = 0;

	function importCSV(start, end, totalRows, attachmentURL) {
		var data = {
			'action': 'import_barcodes_records',
			'start': start,
			'end': end,
			'totalRows': totalRows,
			'attachmentURL': attachmentURL
		};
		start += 10;
		end += 10;
		barCount += 1;
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if (parseInt(totalRows) >= start) {
				bar.animate(barInc * barCount);
				console.log(barInc * barCount);
				importCSV(start, end, parseInt(totalRows), attachmentURL);
			} else {
				bar.animate(1);
			}
		});
	}

	function readURL(input) {
		if (input.files && input.files[0]) {
		  	var reader = new FileReader();
		  	reader.onload = function(e) {
				$('.image-upload-wrap').hide();
				$('.file-upload-content').show();
				$('.image-title').html(input.files[0].name);
		  	};
			reader.readAsDataURL(input.files[0]);
			$(document).find(".file-upload-btn").hide();  
			$(document).find(".submit-file-upload-btn").show();  
		} else {
		  	removeUpload();
		}
	}

	function removeUpload() {
		$('.file-upload-input').replaceWith($('.file-upload-input').clone());
		$('.file-upload-content').hide();
		$('.image-upload-wrap').show();
		$(document).find(".file-upload-btn").show();  
		$(document).find(".submit-file-upload-btn").hide(); 
	}
	
	$(document).ready(function() {
		// Select2 Initialization
		$(document).find(".custom-profiles-settings-table .js-example-basic-single").select2();

		// DataTables Initialization
		$(document).find(".datatable").DataTable();

		if ($(document).find("#progressbar").length > 0) {
			bar = new ProgressBar.Line(progressbar, {
				strokeWidth: 2,
				easing: 'easeInOut',
				duration: 1400,
				color: '#1AA059',
				trailColor: '#fff',
				trailWidth: 1,
				svgStyle: {width: '100%', height: '100%'},
				text: {
					style: {
						// Text color.
						// Default: same as stroke color (options.color)
						color: 'black',
						position: 'absolute',
						right: '50%',
						top: '26%',
						padding: 0,
						margin: 0,
						transform: null
					},
					autoStyleContainer: false
				},
				from: {color: '#1AA059'},
				to: {color: '#1AA059'},
				step: (state, bar) => {
					  bar.setText(Math.round(bar.value() * 100) + ' %');
				}
			});
		}

		// Code for View Profiles Button
		$(document).on("click", ".view-profiles-btn", function() {
			var currentButton = $(this);
			var user_id = $(this).data("user");
			var data = {
				'action': 'get_user_profiles',
				'user_id': user_id
			};
			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			jQuery.post(ajaxurl, data, function(response) {
				$(document).find(".profile-modal tbody").html("");
				var i = 1;
				var response = JSON.parse(response);
				response.forEach(element => {
					var html = "<tr>";
					html += "<td>"+i+"</td>";
					html += "<td><img src='"+element.profilePictureURL+"'></td>";
					html += "<td>"+element.fullName+"</td>";
					html += "<td>"+element.dateOfBirth+"</td>";
					html += "<td>"+element.gender+"</td>";
					html += "<td>"+element.ethnicity+"</td>";
					html += "<td>"+element.height+"</td>";
					html += "<td>"+element.weight+"</td>";
					html += "<td>"+element.barcode_status_sc+"</td>";
					html += "<td><a class='button button-primary' href='"+element.pageURL+"'>View Page</a></td>";
					html += "</tr>";
					$(document).find(".profile-modal tbody").append(html);
					i++;
				});
				currentButton.next().trigger("click");
			});
		});

		$(document).on( 'click', '.upload-csv-file-btn', function(e){
			e.preventDefault();
			var button = $(this),
			custom_uploader = wp.media({
				title: 'Insert CSV',
				library : {
					// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
					type : 'csv'
				},
				button: {
					text: 'Use this file' // button label text
				},
				multiple: false
			}).on('select', function() { // it also has "open" and "close" events
				var attachment = custom_uploader.state().get('selection').first().toJSON();
				$(document).find("#attachment-id").val(attachment.id);
				$(document).find("#attachment-url").val(attachment.url);
				$(document).find("#uploaded-csv").val(attachment.url);
			}).open();
		});

		$(document).on("click", ".import-barcodes-btn", function() {
			var attachmentID = $(document).find("#attachment-id").val();
			var attachmentURL = $(document).find("#attachment-url").val();
			var totalRows = 0;
			if ( attachmentID && attachmentURL ) {
				var data = {
					'action': 'count_file_contents',
					'attachmentID': attachmentID,
					'attachmentURL': attachmentURL
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(ajaxurl, data, function(response) {
					totalRows = parseInt(response);
					if (totalRows <= 10) {
						barInc = 1;
					} else {
						barInc = 10 / totalRows;
					}
					importCSV(1, 10, totalRows, attachmentURL);
				});
			} else {
				alert("Please upload file and then import barcodes.");
				return false;
			}
		});
	});

})( jQuery );
