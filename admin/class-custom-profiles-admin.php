<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/admin
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Profiles_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Profiles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Profiles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-select2-css', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-jquery-modal-css', plugin_dir_url( __FILE__ ) . 'css/jquery.modal.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-dataTables-css', plugin_dir_url( __FILE__ ) . 'css/jquery.dataTables.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-profiles-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Profiles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Profiles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( ! did_action( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		}
		wp_enqueue_script( $this->plugin_name.'-select2-js', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery-modal-js', plugin_dir_url( __FILE__ ) . 'js/jquery.modal.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-progressbar-js', plugin_dir_url( __FILE__ ) . 'js/progressbar.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-dataTables-js', plugin_dir_url( __FILE__ ) . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-profiles-admin.js', array( 'jquery' ), $this->version, false );
	
	}

	public function custom_profiles_plugin_settings() {
		register_setting( 'custom-profiles-settings', 'custom_profiles_settings' );
		register_setting( 'custom-profiles-general-settings', 'custom_profiles_general_settings' );
	}

	public function custom_profiles_plugin_menu() {
		add_menu_page(
			'Custom Profiles Settings',
			'Custom Profiles Settings',
			'manage_options',
			'custom-profiles-settings',
			array($this, 'plugin_settings_callback')
		);
		add_submenu_page(
			'custom-profiles-settings',
			'All Barcodes',
			'All Barcodes',
			'manage_options',
			'custom-profiles-import-barcodes-display',
			array($this, 'plugin_import_barcodes_display_callback')
		);
		add_submenu_page(
			'custom-profiles-settings',
			'Import Barcodes',
			'Import Barcodes',
			'manage_options',
			'custom-profiles-import-barcodes',
			array($this, 'plugin_import_barcodes_callback')
		);
	}

	public function plugin_settings_callback() {
		require('partials/custom-profiles-admin-display.php');
	}

	public function plugin_import_barcodes_display_callback() {
		require('partials/custom-profiles-admin-import-barcodes-display-display.php');
	}

	public function plugin_import_barcodes_callback() {
		require('partials/custom-profiles-admin-import-barcodes-display.php');
	}

	public function check_email_address() {
		$email = $_POST['email'];
		echo email_exists($email);
	}

	public function custom_profiles_register() {
		$email = $_POST['email'];
		$full_name = $_POST['full_name'];
		$username = strstr($email, '@', true);
		$password = $_POST['password'];
		$result = wp_create_user( $username, $password, $email );
		if (is_wp_error($result)) {
			$error = $result->get_error_message();
			$customProfilesSettings = get_option('custom_profiles_settings', array());
            $custom_profiles_register_page_id = $customProfilesSettings['custom_profiles_register_page_id'];
			wp_redirect( get_permalink($custom_profiles_register_page_id) . '?register=false' );
		} else {
			$user = get_user_by('id', $result);
			update_user_meta( $user->ID, 'full_name', $full_name );
			update_user_meta( $user->ID, 'activated', 'false' );
			$customProfilesSettings = get_option('custom_profiles_settings', array());
            $custom_profiles_register_page_id = $customProfilesSettings['custom_profiles_register_page_id'];
			wp_redirect( get_permalink($custom_profiles_register_page_id) . '?register=true&user=' . $user->ID );
		}
	}

	public function custom_profiles_login() {
		$email = $_POST['email'];
		$password = $_POST['password'];
		// Authorize
		$auth = wp_authenticate( $email, $password );
		// Error checking
		if ( is_wp_error( $auth ) ) {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
            $custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
			wp_redirect( get_permalink($custom_profiles_login_page_id) . '?success=false' );
		}
		else {
			$user_id = $auth->ID;
			$user_meta = get_userdata($user_id);
			$user_roles = $user_meta->roles;
			if (in_array('lab-manager', $user_roles)) {
				clean_user_cache($user_id);
				wp_clear_auth_cookie();
				wp_set_current_user($user_id);
				wp_set_auth_cookie($user_id, true, false);
				$user = get_user_by('id', $user_id);
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_barcode_dashboard_page_id = $customProfilesSettings['custom_profiles_barcode_dashboard_page_id'];
				wp_redirect( get_permalink($custom_profiles_barcode_dashboard_page_id) );
			}
			if (in_array('subscriber', $user_roles)) {
				$activated = get_user_meta( $user_id, 'activated', true );
				if ( isset($activated) && !empty($activated) && $activated == 'false' ) {
					$customProfilesSettings = get_option('custom_profiles_settings', array());
					$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
					wp_redirect( get_permalink($custom_profiles_login_page_id) . '?activated=false' );
				} else {
					clean_user_cache($user_id);
					wp_clear_auth_cookie();
					wp_set_current_user($user_id);
					wp_set_auth_cookie($user_id, true, false);
					$user = get_user_by('id', $user_id);
					update_user_caches($user);
					$args = array(
						'post_type'=> 'custom-profile',
						'orderby'    => 'ID',
						'author'    => $user_id,
						'post_status' => 'publish',
						'order'    => 'DESC',
						'posts_per_page' => -1 // this will retrive all the post that is published 
					);
					$profiles = get_posts( $args );
					$firstProfile = $profiles[0];
					$args = array(
						'post_type'=> 'page',
						'orderby'    => 'ID',
						'post_status' => 'publish',
						'order'    => 'DESC',
						'posts_per_page' => 1, // this will retrive all the post that is published
						'meta_query' => array(
							array(
								'key' => 'profile_id',
								'value' => $firstProfile->ID,
								'compare' => '=',
							)
						)
					);
					$page = get_posts( $args )[0];
					wp_redirect( get_permalink( $page->ID ) );
				}
			}
		}
	}

	public function custom_profiles_change_password() {
		$user_id = get_current_user_id();
		$password = $_POST['password'];
		$repeat_password = $_POST['repeat_password'];
		wp_set_password( $password, (int) $user_id );
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
		wp_redirect( get_permalink($custom_profiles_login_page_id) . '?change-password=true' );
	}

	public function custom_profiles_add_new_profile() {
		$i = 1;
		$wordpress_upload_dir = wp_upload_dir();
		$profile_picture = $_FILES['profile_picture'];
		$new_file_path = $wordpress_upload_dir['path'] . '/' . $profile_picture['name'];
		$new_file_mime = mime_content_type( $profile_picture['tmp_name'] );
		if( empty( $profile_picture ) ) {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
			wp_redirect( get_permalink($custom_profiles_add_new_profile_page_id) . '?upload-message=File is not selected.' );
		}
		if( $profile_picture['error'] ) {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
			wp_redirect( get_permalink($custom_profiles_add_new_profile_page_id) . '?upload-message=' . $profile_picture['error'] );
		}
		if( $profile_picture['size'] > wp_max_upload_size() ) {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
			wp_redirect( get_permalink($custom_profiles_add_new_profile_page_id) . '?upload-message=It is too large than expected.' );
		}
		if( !in_array( $new_file_mime, get_allowed_mime_types() ) ) {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
			wp_redirect( get_permalink($custom_profiles_add_new_profile_page_id) . '?upload-message=WordPress does not allow this type of uploads.' );
		}
		while( file_exists( $new_file_path ) ) {
			$i++;
			$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profile_picture['name'];
		}
		// looks like everything is OK
		if( move_uploaded_file( $profile_picture['tmp_name'], $new_file_path ) ) {
			$upload_id = wp_insert_attachment( array(
				'guid'           => $new_file_path, 
				'post_mime_type' => $new_file_mime,
				'post_title'     => preg_replace( '/\.[^.]+$/', '', $profile_picture['name'] ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			), $new_file_path );
			// wp_generate_attachment_metadata() won't work if you do not include this file
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			// Generate and save the attachment metas into the database
			wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
			
			// Creating Children Profile
			$new_post = array(
				'post_title' => rand(1000000000, 9999999999),
				'post_content' => '',
				'post_status' => 'publish',
				'post_date' => date('Y-m-d H:i:s'),
				'post_author' => get_current_user_id(),
				'post_type' => 'custom-profile',
			);
			$post_id = wp_insert_post( $new_post );
			update_post_meta( $post_id, 'full_name', $_POST['full_name'] );
			update_post_meta( $post_id, 'date_of_birth', $_POST['date_of_birth'] );
			update_post_meta( $post_id, 'gender', $_POST['gender'] );
			update_post_meta( $post_id, 'ethnicity', $_POST['ethnicity'] );
			update_post_meta( $post_id, 'height', $_POST['height'] );
			update_post_meta( $post_id, 'weight', $_POST['weight'] );
			update_post_meta( $post_id, 'profile_picture', $upload_id );
			update_post_meta( $post_id, 'barcode', $_POST['barcode'] );
			update_post_meta( $post_id, 'barcode_status', 'inactive' );
			$args = array(
				'role'    => 'lab-manager',
				'orderby' => 'user_nicename',
				'order'   => 'ASC'
			);
			$users = get_users( $args );
			if (isset($users) && is_array($users) && count($users) > 0) {
				foreach ($users as $user) {
					$userData = get_userdata($user->ID);
					$email = $userData->user_email;
					$to = $email;
					// $to = 'talhamirza2@gmail.com';
					$subject = 'New Barcode Applied';
					$from = get_option('admin_email');
					// To send HTML mail, the Content-type header must be set
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();
					// Compose a simple HTML email message
					$message = '';
					$message .= '<h3>Hi Lab Manager!</h3>';
					$message .= '<p style="font-weight: bold;">New barcode has been applied. Barcode is: '.$_POST['barcode'].'. Please update the status of it.</p>';
					$message .= '<p>Thanks for your attention!</p>';
					$message .= '';
					// Sending Mail
					wp_mail($to, $subject, $message, $headers);
				}
			}
			
			// Create Wordpress Page for New Profile
			$page_details = array(
				'post_title'    => $_POST['full_name'] . ' Page',
				'post_content'  => 'Content of your page',
				'post_status'   => 'publish',
				'post_author'   => 1,
				'post_type' => 'page'
			);
			$page_id = wp_insert_post( $page_details );
			update_post_meta( $page_id, 'profile_id', $post_id );
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_manage_users_page_id = $customProfilesSettings['custom_profiles_manage_users_page_id'];
			wp_redirect( get_permalink($custom_profiles_manage_users_page_id) . '?profile-added=true' );
		} else {
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
			wp_redirect( get_permalink($custom_profiles_add_new_profile_page_id) . '?success=false' );
		}
	}

	public function custom_profiles_edit_profile() {
		if ( !file_exists($_FILES['profile_picture']['tmp_name']) || !is_uploaded_file($_FILES['profile_picture']['tmp_name']) ) {
			update_post_meta( (int) $_POST['profile_id'], 'full_name', $_POST['full_name'] );
			update_post_meta( (int) $_POST['profile_id'], 'date_of_birth', $_POST['date_of_birth'] );
			update_post_meta( (int) $_POST['profile_id'], 'gender', $_POST['gender'] );
			update_post_meta( (int) $_POST['profile_id'], 'ethnicity', $_POST['ethnicity'] );
			update_post_meta( (int) $_POST['profile_id'], 'height', $_POST['height'] );
			update_post_meta( (int) $_POST['profile_id'], 'weight', $_POST['weight'] );
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_manage_users_page_id = $customProfilesSettings['custom_profiles_manage_users_page_id'];
			wp_redirect( get_permalink($custom_profiles_manage_users_page_id) . '?profile-edited=true' );
		} else {
			$i = 1;
			$wordpress_upload_dir = wp_upload_dir();
			$profile_picture = $_FILES['profile_picture'];
			$new_file_path = $wordpress_upload_dir['path'] . '/' . $profile_picture['name'];
			$new_file_mime = mime_content_type( $profile_picture['tmp_name'] );
			if( empty( $profile_picture ) ) {
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
				wp_redirect( get_permalink($custom_profiles_edit_profile_page_id) . '?upload-message=File is not selected.' );
			}
			if( $profile_picture['error'] ) {
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
				wp_redirect( get_permalink($custom_profiles_edit_profile_page_id) . '?upload-message=' . $profile_picture['error'] );
			}
			if( $profile_picture['size'] > wp_max_upload_size() ) {
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
				wp_redirect( get_permalink($custom_profiles_edit_profile_page_id) . '?upload-message=It is too large than expected.' );
			}
			if( !in_array( $new_file_mime, get_allowed_mime_types() ) ) {
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
				wp_redirect( get_permalink($custom_profiles_edit_profile_page_id) . '?upload-message=WordPress does not allow this type of uploads.' );
			}
			while( file_exists( $new_file_path ) ) {
				$i++;
				$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profile_picture['name'];
			}
			// looks like everything is OK
			if( move_uploaded_file( $profile_picture['tmp_name'], $new_file_path ) ) {
				$upload_id = wp_insert_attachment( array(
					'guid'           => $new_file_path, 
					'post_mime_type' => $new_file_mime,
					'post_title'     => preg_replace( '/\.[^.]+$/', '', $profile_picture['name'] ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				), $new_file_path );
				// wp_generate_attachment_metadata() won't work if you do not include this file
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				// Generate and save the attachment metas into the database
				wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
				
				// Editing Children Profile
				update_post_meta( (int) $_POST['profile_id'], 'full_name', $_POST['full_name'] );
				update_post_meta( (int) $_POST['profile_id'], 'date_of_birth', $_POST['date_of_birth'] );
				update_post_meta( (int) $_POST['profile_id'], 'gender', $_POST['gender'] );
				update_post_meta( (int) $_POST['profile_id'], 'ethnicity', $_POST['ethnicity'] );
				update_post_meta( (int) $_POST['profile_id'], 'height', $_POST['height'] );
				update_post_meta( (int) $_POST['profile_id'], 'weight', $_POST['weight'] );
				update_post_meta( (int) $_POST['profile_id'], 'profile_picture', $upload_id );
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_manage_users_page_id = $customProfilesSettings['custom_profiles_manage_users_page_id'];
				wp_redirect( get_permalink($custom_profiles_manage_users_page_id) . '?profile-edited=true' );
			} else {
				$customProfilesSettings = get_option('custom_profiles_settings', array());
				$custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
				wp_redirect( get_permalink($custom_profiles_edit_profile_page_id) . '?success=false' );
			}
		}
	}

	public function new_modify_user_table( $column ) {
		$column['profile-btn'] = 'Profiles';
		return $column;
	}

	public function new_modify_user_table_row( $val, $column_name, $user_id ) {
		if ($column_name == 'profile-btn') {
			$args = array(
				'post_type'=> 'custom-profile',
				'orderby'    => 'ID',
				'author'    => $user_id,
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1 // this will retrive all the post that is published 
			);
			$profiles = get_posts( $args );
			if (isset($profiles) && is_array($profiles) && count($profiles) > 0) {
				return '<button type="button" class="button button-primary view-profiles-btn" data-user="'.$user_id.'">View Profiles</button><a href="#ex1" rel="modal:open" style="display: none;">Open Modal</a>';
			} else {
				return 'No profiles found.';
			}
		}
		return $val;
	}

	public function lh_add_livechat_js_code() {
		$html = '<div id="ex1" class="modal profile-modal">
			<table>
				<thead>
					<th>#</th>
					<th>Image</th>
					<th>Full Name</th>
					<th>Date of Birth</th>
					<th>Gender</th>
					<th>Ethnicity</th>
					<th>Height</th>
					<th>Weight</th>
					<th>Barcode Status SC</th>
					<th>Actions</th>
				</thead>
				<tbody></tbody>
				<tfoot>
					<th>#</th>
					<th>Image</th>
					<th>Full Name</th>
					<th>Date of Birth</th>
					<th>Gender</th>
					<th>Ethnicity</th>
					<th>Height</th>
					<th>Weight</th>
					<th>Barcode Status SC</th>
					<th>Actions</th>
				</tfoot>
			</table>
		</div>';
		echo $html;
	}

	public function get_user_profiles() {
		$user_id = (int) $_POST['user_id'];
		$args = array(
			'post_type'=> 'custom-profile',
			'orderby'    => 'ID',
			'author'    => $user_id,
			'post_status' => 'publish',
			'order'    => 'DESC',
			'posts_per_page' => -1 // this will retrive all the post that is published 
		);
		$profiles = get_posts( $args );
		$profiles_array = array();
		foreach ($profiles as $profile) {
			$array = array();
			$array['fullName'] = get_post_meta( $profile->ID, 'full_name', true );
			$array['dateOfBirth'] = get_post_meta( $profile->ID, 'date_of_birth', true );
			$array['gender'] = get_post_meta( $profile->ID, 'gender', true );
			$array['ethnicity'] = get_post_meta( $profile->ID, 'ethnicity', true );
			$array['height'] = get_post_meta( $profile->ID, 'height', true );
			$array['weight'] = get_post_meta( $profile->ID, 'weight', true );
			$array['barcode_status_sc'] = '[custom-profiles-barcode-status id="'.$profile->ID.'"]';
			$profilePicture = (int) get_post_meta( $profile->ID, 'profile_picture', true );
			if (isset($profilePicture) && !empty($profilePicture)) {
				$array['profilePictureURL'] = wp_get_attachment_url($profilePicture);
			} else {
				$array['profilePictureURL'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU';
			}
			$args = array(
                'post_type'=> 'page',
                'orderby'    => 'ID',
                'post_status' => 'publish',
                'order'    => 'DESC',
                'posts_per_page' => 1, // this will retrive all the post that is published
                'meta_query' => array(
                    array(
                        'key' => 'profile_id',
                        'value' => $profile->ID,
                        'compare' => '=',
                    )
                )
            );
			$page = get_posts( $args )[0];
			$array['pageURL'] = get_permalink($page->ID);
			array_push($profiles_array, $array);
		}
		echo json_encode($profiles_array);
		exit;
	}

	public function count_file_contents() {
		$file = file($_POST['attachmentURL'], FILE_SKIP_EMPTY_LINES);
		echo count($file);
		exit;
	}

	public function import_barcodes_records() {
		$start = (int) $_POST['start'];
		$end = (int) $_POST['end'];
		$count = 1;
		$file = fopen($_POST['attachmentURL'], 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			if ($count >= $start && $count <= $end && isset($line[0]) && !empty($line[0])) {
				global $user_ID;
				$new_post = array(
					'post_title' => $line[0],
					'post_status' => 'publish',
					'post_date' => date('Y-m-d H:i:s'),
					'post_author' => $user_ID,
					'post_type' => 'custom-barcode',
				);
				$post_id = wp_insert_post($new_post);
				// Update Meta Data
				update_post_meta( $post_id, 'used', false);
			}
			$count++;
		}
		fclose($file);
		exit;
	}

	function get_post_by_title($page_title, $post_type ='post' , $output = OBJECT) {
		global $wpdb;
			$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));
			if ( $post )
				return get_post($post, $output);
	
		return null;
	}

	public function check_barcode() {
		$barcode = $_POST['barcode'];
		$barcodePost = get_page_by_title($barcode, '', 'custom-barcode');
		if ($barcodePost) {
			$used = get_post_meta( $barcodePost->ID, 'used', true );
			if ($used) {
				echo "Used";
			} else {
				update_post_meta( $barcodePost->ID, 'used', true );
				echo "Success";
			}
		} else {
			echo  "NotFound";
		}
		exit;
	}

	public function custom_profiles_activate_kit() {
		$userID = $_POST['userID'];
		$full_name = $_POST['full_name'];
		$barcode = $_POST['barcode'];
		update_user_meta( (int) $userID, 'activated', 'true' );
		$new_post = array(
			'post_title' => rand(1000000000, 9999999999),
			'post_content' => '',
			'post_status' => 'publish',
			'post_date' => date('Y-m-d H:i:s'),
			'post_author' => (int) $userID,
			'post_type' => 'custom-profile',
		);
		$post_id = wp_insert_post( $new_post );
		update_post_meta( $post_id, 'full_name', $full_name );
		update_post_meta( $post_id, 'date_of_birth', '' );
		update_post_meta( $post_id, 'gender', '' );
		update_post_meta( $post_id, 'ethnicity', '' );
		update_post_meta( $post_id, 'height', '' );
		update_post_meta( $post_id, 'weight', '' );
		update_post_meta( $post_id, 'profile_picture', '' );
		update_post_meta( $post_id, 'barcode', $barcode );
		update_post_meta( $post_id, 'barcode_status', 'inactive' );
		$args = array(
			'role'    => 'lab-manager',
			'orderby' => 'user_nicename',
			'order'   => 'ASC'
		);
		$users = get_users( $args );
		if (isset($users) && is_array($users) && count($users) > 0) {
			foreach ($users as $user) {
				$userData = get_userdata($user->ID);
				$email = $userData->user_email;
				$to = $email;
				// $to = 'talhamirza2@gmail.com';
				$subject = 'New Barcode Applied';
				$from = get_option('admin_email');
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// Create email headers
				$headers .= 'From: '.$from."\r\n".
					'Reply-To: '.$from."\r\n" .
					'X-Mailer: PHP/' . phpversion();
				// Compose a simple HTML email message
				$message = '';
				$message .= '<h3>Hi Lab Manager!</h3>';
				$message .= '<p style="font-weight: bold;">New barcode has been applied. Barcode is: '.$barcode.'. Please update the status of it.</p>';
				$message .= '<p>Thanks for your attention!</p>';
				$message .= '';
				// Sending Mail
				wp_mail($to, $subject, $message, $headers);
			}
		}
		$page_details = array(
			'post_title'    => $full_name . ' Page',
			'post_content'  => 'Content of your page',
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_type' => 'page'
		);
		$page_id = wp_insert_post( $page_details );
		update_post_meta( $page_id, 'profile_id', $post_id );
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
		wp_redirect( get_permalink($custom_profiles_login_page_id) . '?welcome=true' );
	}

	public function wpse27856_set_content_type() {
		return "text/html";
	}

	public function custom_profiles_forgot_password() {
		$user = get_user_by( 'email', $_POST['email'] );
		if ( $user ) {
			$full_name = get_user_meta( $user->ID, 'full_name', true );
			$to = $_POST['email'];
			$subject = 'Forgot Password';
			$from = get_option('admin_email');
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Create email headers
			$headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			// Compose a simple HTML email message
			$message = '';
			$message .= '<h3>Hi '.$full_name.'!</h3>';
			$message .= '<p style="font-weight: bold;">Your password reset link is following.</p>';
			$customProfilesSettings = get_option('custom_profiles_settings', array());
			$custom_profiles_reset_password_page_id = $customProfilesSettings['custom_profiles_reset_password_page_id'];
			$message .= '<p>Please go to the link: <a href="'.get_permalink($custom_profiles_reset_password_page_id).'?user='.$user->ID.'">Reset Password</a> to reset your password.</p>';
			$message .= '<p>Thanks for your attention!</p>';
			$message .= '';
			// Sending Mail
			wp_mail($to, $subject, $message, $headers);
		}
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_forgot_password_page_id = $customProfilesSettings['custom_profiles_forgot_password_page_id'];
		wp_redirect( get_permalink($custom_profiles_forgot_password_page_id) . '?success=true' );
	}

	public function custom_profiles_reset_password() {
		$userID = $_POST['userID'];
		$password = $_POST['password'];
		wp_set_password( $password, (int) $userID );
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
		wp_redirect( get_permalink($custom_profiles_login_page_id) . '?change-password=true' );
	}

	public function update_barcode_status() {
		$profile_id = $_POST['profile_id'];
		$status = $_POST['status'];
		update_post_meta( (int) $profile_id, 'barcode_status', $_POST['status'] );
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_barcode_dashboard_page_id = $customProfilesSettings['custom_profiles_barcode_dashboard_page_id'];
		wp_redirect( get_permalink($custom_profiles_barcode_dashboard_page_id) . '?success=true' );
	}

}
