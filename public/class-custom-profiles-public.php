<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/public
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Profiles_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Profiles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Profiles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-fontawesome-css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-dataTables-css', plugin_dir_url( __FILE__ ) . 'css/jquery.dataTables.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-profiles-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Profiles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Profiles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-dataTables-js', plugin_dir_url( __FILE__ ) . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-profiles-public.js', array( 'jquery' ), $this->version, false );
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$custom_profiles_activate_barcode_page_id = $customProfilesSettings['custom_profiles_activate_barcode_page_id'];
		$custom_profiles_activate_barcode_page_url = get_permalink( $custom_profiles_activate_barcode_page_id );
		wp_localize_script( $this->plugin_name, 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'activate_barcode_page_url' => $custom_profiles_activate_barcode_page_url ) );

	}

	public function add_shortcodes() {
		add_shortcode( 'custom-profiles-register', array($this, 'custom_profiles_register_shortcode') );
		add_shortcode( 'custom-profiles-login', array($this, 'custom_profiles_login_shortcode') );
		add_shortcode( 'custom-profiles-activate-kit', array($this, 'custom_profiles_activate_kit_shortcode') );
		add_shortcode( 'custom-profiles-settings', array($this, 'custom_profiles_settings_shortcode') );
		add_shortcode( 'custom-profiles-change-password', array($this, 'custom_profiles_change_password_shortcode') );
		add_shortcode( 'custom-profiles-manage-users', array($this, 'custom_profiles_manage_users_shortcode') );
		add_shortcode( 'custom-profiles-add-new-profile', array($this, 'custom_profiles_add_new_profile_shortcode') );
		add_shortcode( 'custom-profiles-edit-profile', array($this, 'custom_profiles_edit_profile_shortcode') );
		add_shortcode( 'custom-profiles-forgot-password', array($this, 'custom_profiles_forgot_password_shortcode') );
		add_shortcode( 'custom-profiles-reset-password', array($this, 'custom_profiles_reset_password_shortcode') );
		add_shortcode( 'custom-profiles-barcode-dashboard', array($this, 'custom_profiles_barcode_dashboard_shortcode') );
		add_shortcode( 'custom-profiles-barcode-status', array($this, 'custom_profiles_barcode_status_shortcode') );
	}

	public function custom_profiles_register_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-register-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_login_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-login-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_activate_kit_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-activate-kit-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_settings_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-settings-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_change_password_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-change-password-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_manage_users_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-manage-users-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_add_new_profile_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-add-new-profile-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_edit_profile_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-edit-profile-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_reset_password_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-reset-password-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_forgot_password_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-forgot-password-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_barcode_dashboard_shortcode() {
		ob_start();
        include_once 'partials/custom-profiles-public-barcode-dashboard-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_profiles_barcode_status_shortcode( $attr ) {
		ob_start();
		$profile_id = $attr['id'];
		include_once 'partials/custom-profiles-public-barcode-status-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function new_nav_menu_items( $items ) {
		$customProfilesSettings = get_option('custom_profiles_settings', array());
		$link = '';
		$userID = get_current_user_id();
		$user_meta = get_userdata($userID);
		$user_roles = $user_meta->roles;
		if (isset($userID) && !empty($userID)) {
			if (in_array('lab-manager', $user_roles)) {
				$custom_profiles_barcode_dashboard_page_id = $customProfilesSettings['custom_profiles_barcode_dashboard_page_id'];
				$link = '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item nav-item"><a title="Home" href="'.get_permalink($custom_profiles_barcode_dashboard_page_id).'" class="nav-link mt-1" aria-current="page">Barcode Dashboard</a></li>';
				$link .= '<li class="menu-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Logout"><a class="btn btn-dark btn-circle btn-sm mt-1 ml-1" href="'.wp_logout_url( home_url()).'"><i class="fa fa-times mr-0"></i></a></li>';
				$items = $items . $link;
				return $items;
			}
			if (in_array('subscriber', $user_roles)) {
				$activated = get_user_meta( $userID, 'activated', true );
				$full_name = get_user_meta( $userID, 'full_name', true );
				$args = array(
					'post_type'=> 'custom-profile',
					'orderby'    => 'ID',
					'author'    => get_current_user_id(),
					'post_status' => 'publish',
					'order'    => 'DESC',
					'posts_per_page' => -1 // this will retrive all the post that is published 
				);
				$profiles = get_posts( $args );
				if (isset($userID) && !empty($userID)) {
					if (isset($activated) && !empty($activated)) {
						$link = '<div class="dropdown">
							<button class="btn btn-light dropdown-toggle profile-switch-btn" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
							'.$full_name.'
							</button>
							<ul class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenuButton">';
							foreach ($profiles as $profile) {
								$args = array(
									'post_type'=> 'page',
									'orderby'    => 'ID',
									'post_status' => 'publish',
									'order'    => 'DESC',
									'posts_per_page' => 1, // this will retrive all the post that is published
									'meta_query' => array(
										array(
											'key' => 'profile_id',
											'value' => $profile->ID,
											'compare' => '=',
										)
									)
								);
								$page = get_posts( $args )[0];
								$full_name = get_post_meta( $profile->ID, 'full_name', true );
								global $post;
								$current_page_id = $post->ID;
								$active = '';
								if ($current_page_id == $page->ID) {
									$active = 'active-user';
								}
								$profile_picture = get_post_meta( $profile->ID, 'profile_picture', true );
								if (isset($profile_picture) && !empty($profile_picture)) {
									$profile_picture = wp_get_attachment_url((int) $profile_picture);
								} else {
									$profile_picture = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU';
								}
						$link .= '<li><a class="dropdown-item '.$active.'" data-profile-picture="'.$profile_picture.'" href="'.get_permalink($page->ID).'">'.$full_name.'</a></li>';
							}
						$link .= '</ul>
						</div>';
						$link .= '<div class="custom-profiles-profile-picture-nav-menu profile-picture">
							<div class="circle">
								<img class="profile-pic nav-item-profile-picture" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU">
							</div>
						</div>';
						$customProfilesSettings = get_option('custom_profiles_settings', array());
						$custom_profiles_add_new_profile_page_id = $customProfilesSettings['custom_profiles_add_new_profile_page_id'];
						$custom_profiles_settings_page_id = $customProfilesSettings['custom_profiles_settings_page_id'];
						$link .= '<li class="menu-item pr-0" data-bs-toggle="tooltip" data-bs-placement="top" title="Add Profile"><a class="btn btn-primary btn-circle btn-sm mt-1 ml-3" href="'.get_permalink($custom_profiles_add_new_profile_page_id).'"><i class="fa fa-plus mr-0"></i></a></li>';
						$link .= '<li class="menu-item pr-0" data-bs-toggle="tooltip" data-bs-placement="top" title="Go to Settings"><a class="btn btn-dark btn-circle btn-sm mt-1 ml-3" href="'.get_permalink($custom_profiles_settings_page_id).'"><i class="fa fa-cog mr-0"></i></a></li>';
						$link .= '<li class="menu-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Logout"><a class="btn btn-dark btn-circle btn-sm mt-1 ml-3" href="'.wp_logout_url( home_url()).'"><i class="fa fa-times mr-0"></i></a></li>';
					}
				} else {
					$custom_profiles_register_page_id = $customProfilesSettings['custom_profiles_register_page_id'];
					$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
					$link = '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item nav-item"><a title="Home" href="'.get_permalink($custom_profiles_login_page_id).'" class="nav-link mt-1" aria-current="page">Login</a></li>';
					$link .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item nav-item"><a title="Home" href="'.get_permalink($custom_profiles_register_page_id).'" class="nav-link mt-1" aria-current="page">Signup</a></li>';
				}
				$items = $items . $link;
				return $items;
			}
		} else {
			$custom_profiles_register_page_id = $customProfilesSettings['custom_profiles_register_page_id'];
			$custom_profiles_login_page_id = $customProfilesSettings['custom_profiles_login_page_id'];
			$link = '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item nav-item"><a title="Home" href="'.get_permalink($custom_profiles_login_page_id).'" class="nav-link mt-1" aria-current="page">Login</a></li>';
			$link .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item nav-item"><a title="Home" href="'.get_permalink($custom_profiles_register_page_id).'" class="nav-link mt-1" aria-current="page">Signup</a></li>';
			$items = $items . $link;
			return $items;
		}
	}

}
