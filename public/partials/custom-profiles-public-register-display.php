<div class="custom-profiles-register-shortcode">
    <h3>REGISTER NEW ACCOUNT</h3>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="register-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_register">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Registration successfully done!</strong> Redirecting...
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" data-name="Email" class="form-control required" id="email" name="email" placeholder="Enter Email">
        </div>
        <div class="mb-3">
            <label for="full-name" class="form-label">Full Name</label>
            <input type="text" data-name="Full Name" class="form-control required" id="full-name" name="full_name" placeholder="Enter Full Name">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" data-name="Password" class="form-control required" id="password" name="password" placeholder="Enter Password">
        </div>
        <div class="mb-3">
            <label for="retype-password" class="form-label">Retype Password</label>
            <input type="password" data-name="Retype Password" class="form-control required" id="retype-password" name="retype_password" placeholder="Retype Password">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block register-btn">Register</button>
    </form>
</div>