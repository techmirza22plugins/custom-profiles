<div class="custom-profiles-activate-kit-shortcode">
    <h3>ACTIVATE KIT</h3>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Barcode activated successfully!</strong> Redirecting to the login page...
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="activate-kit-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_activate_kit">
        <input type="hidden" name="userID" value="<?php echo $_GET['user']; ?>">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <div class="mb-3">
            <label for="full_name" class="form-label">Full Name (First Child)</label>
            <input type="text" data-name="Full Name" class="form-control required" id="full_name" name="full_name" placeholder="Enter Full Name">
        </div>
        <div class="mb-3">
            <label for="barcode" class="form-label">Barcode</label>
            <input type="text" data-name="Barcode" class="form-control required" id="barcode" name="barcode" placeholder="Enter Barcode From Your Box">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block activate-kit-btn">Submit</button>
    </form>
</div>