<div class="custom-profiles-reset-password-shortcode">
    <h3>RESET PASSWORD</h3>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="reset-password-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_reset_password">
        <input type="hidden" name="userID" value="<?php echo $_GET['user']; ?>">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" data-name="Password" class="form-control required" id="password" name="password" placeholder="Enter Password">
        </div>
        <div class="mb-3">
            <label for="retype-password" class="form-label">Retype Password</label>
            <input type="password" data-name="Retype Password" class="form-control required" id="retype-password" name="retype_password" placeholder="Retype Password">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block reset-password-btn">Submit</button>
    </form>
</div>