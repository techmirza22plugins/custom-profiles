<div class="custom-profiles-forgot-password-shortcode">
    <h3>FORGOT PASSWORD</h3>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Success!</strong> Please check your inbox and reset the password.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="forgot-password-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_forgot_password">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="text" data-name="Email" class="form-control required" id="email" name="email" placeholder="Enter Email">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block forgot-password-btn">Submit</button>
    </form>
</div>