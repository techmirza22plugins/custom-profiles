<?php
    $args = array(
        'post_type'=> 'custom-profile',
        'orderby'    => 'ID',
        'author'    => get_current_user_id(),
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1 // this will retrive all the post that is published 
    );
    $profiles = get_posts( $args );
    $customProfilesSettings = get_option('custom_profiles_settings', array());
    $custom_profiles_edit_profile_page_id = $customProfilesSettings['custom_profiles_edit_profile_page_id'];
?>
<div class="custom-profiles-manage-users-shortcode mx-auto">
    <h3 class="text-center">MANAGE USERS</h3>
    <?php if (isset($_GET['profile-edited']) && !empty($_GET['profile-edited']) && $_GET['profile-edited'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Profile edited successfully!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($_GET['profile-added']) && !empty($_GET['profile-added']) && $_GET['profile-added'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Profile added successfully!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($profiles) && is_array($profiles) && count($profiles) > 0) { ?>
        <?php foreach ($profiles as $profile) {
            $args = array(
                'post_type'=> 'page',
                'orderby'    => 'ID',
                'post_status' => 'publish',
                'order'    => 'DESC',
                'posts_per_page' => 1, // this will retrive all the post that is published
                'meta_query' => array(
                    array(
                        'key' => 'profile_id',
                        'value' => $profile->ID,
                        'compare' => '=',
                    )
                )
            );
            $page = get_posts( $args )[0]; ?>
            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8 pl-4">
                            <?php echo get_post_meta($profile->ID, 'full_name', true); ?>
                        </div>
                        <div class="col-4 text-right pr-4">
                            <a href="<?php echo get_permalink($custom_profiles_edit_profile_page_id) . '?ID=' . $profile->ID; ?>"><i class="fa fa-lg fa-user mr-3" aria-hidden="true"></i></a>
                            <a href="<?php echo get_permalink($page->ID); ?>"><i class="fa fa-lg fa-file-text" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-8 mx-auto">
                        NO PROFILES CREATED YET.
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>