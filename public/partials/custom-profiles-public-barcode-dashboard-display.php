<?php
    $args = array(
        'post_type'=> 'custom-profile',
        'orderby'    => 'ID',
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1, // this will retrive all the post that is published
        'meta_query'        => array(
            array(
                'key'       => 'barcode_status',
                'value'     => 'inactive'
            )
        ),
    );
    $inactive_profiles = get_posts( $args );
    $args = array(
        'post_type'=> 'custom-profile',
        'orderby'    => 'ID',
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1, // this will retrive all the post that is published
        'meta_query'        => array(
            array(
                'key'       => 'barcode_status',
                'value'     => 'inactive',
                'compare'   => '!='
            )
        ),
    );
    $active_profiles = get_posts( $args );
?>
<style>
    table.dataTable thead th, table.dataTable thead td {
        text-align: left !important;
        padding-left: 10px !important;
    }
    table.dataTable tfoot th, table.dataTable tfoot td {
        text-align: left !important;
        padding-left: 10px !important;
    }
</style>
<div class="custom-profiles-barcode-dashbaord-shortcode">
    <h3>Barcode Dashboard</h3>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Barcode status has been updated successfully!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <h4>New Barcodes</h4>
    <table id="example" class="display datatable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($inactive_profiles) && is_array($inactive_profiles) && count($inactive_profiles) > 0) { ?>
                <?php $count = 0; foreach ($inactive_profiles as $profile) { ?>
                    <tr data-profile-id="<?php echo $profile->ID; ?>">
                        <td><?php echo ++$count; ?></td>
                        <td><?php echo get_post_meta( $profile->ID, 'barcode', true ); ?></td>
                        <td><span class="badge badge-danger">Not Set</span></td>
                        <td><button class="btn btn-primary modal-open-btn btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">Update Status</button></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
    <h4>Verified Barcodes</h4>
    <table id="example1" class="display datatable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($active_profiles) && is_array($active_profiles) && count($active_profiles) > 0) { ?>
                <?php $count = 0; foreach ($active_profiles as $profile) { ?>
                    <tr data-profile-id="<?php echo $profile->ID; ?>">
                        <td><?php echo ++$count; ?></td>
                        <td><?php echo get_post_meta( $profile->ID, 'barcode', true ); ?></td>
                        <td>
                            <?php $barcode_status = get_post_meta( $profile->ID, 'barcode_status', true ); ?>
                            <span class="badge badge-success" data-barcode-status="<?php echo $barcode_status; ?>"><?php echo $barcode_status; ?></span>
                        </td>
                        <td><button class="btn btn-primary modal-open-btn btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">Update Status</button></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Barcode</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Status</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="update-status">
                        <input type="hidden" name="action" value="update_barcode_status">
                        <input type="hidden" name="profile_id" value="">
                        <div class="mb-3">
                            <label for="barcode" class="form-label">Barcode</label>
                            <input type="text" data-name="Barcode" class="form-control required" id="barcode" name="password" placeholder="Enter Barcode" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="Arrived at Lab">Arrived at Lab</option>
                                <option value="DNA Extracted">DNA Extracted</option>
                                <option value="DNA Analysed">DNA Analysed</option>
                                <option value="Report Processed">Report Processed</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary barcode-status-update-btn">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>