<?php
    if (isset($_GET['ID']) && !empty($_GET['ID']) && (int) $_GET['ID'] > 0) {
        $profile = get_post((int) $_GET['ID']);
        $full_name = get_post_meta( $profile->ID, 'full_name', true );
		$date_of_birth = get_post_meta( $profile->ID, 'date_of_birth', true );
		$gender = get_post_meta( $profile->ID, 'gender', true );
		$ethnicity = get_post_meta( $profile->ID, 'ethnicity', true );
		$height = get_post_meta( $profile->ID, 'height', true );
		$weight = get_post_meta( $profile->ID, 'weight', true );
        $profile_picture = get_post_meta( $profile->ID, 'profile_picture', true );
        if (isset($profile_picture) && !empty($profile_picture)) {
            $profile_picture = wp_get_attachment_url((int) $profile_picture);
        } else {
            $profile_picture = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU';
        }
    }
?>
<div class="custom-profiles-edit-profile-shortcode">
    <h3>EDIT PROFILE</h3>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="edit-profile-form" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="action" value="custom_profiles_edit_profile">
        <input type="hidden" name="profile_id" value="<?php echo $profile->ID; ?>">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Profile edited successfully!</strong> Redirecting...
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['upload-message']) && !empty($_GET['upload-message'])) : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Error!</strong> <?php echo $_GET['upload-message']; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <div class="custom-profiles-profile-picture profile-picture mx-auto mb-3">
            <div class="circle">
                <img class="profile-pic" src="<?php echo $profile_picture; ?>">
            </div>
            <div class="p-image">
                <button type="button" class="btn btn-dark btn-circle btn-sm btn-upload-circle"><i class="fa fa-camera upload-button mr-0"></i></button>
                <input class="file-upload form-control" name="profile_picture" type="file" accept="image/*"/>
            </div>
        </div>
        <div class="mb-3">
            <label for="full-name" class="form-label">Full Name</label>
            <input type="text" data-name="Full Name" class="form-control required" id="full-name" name="full_name" placeholder="Enter Full Name" value="<?php echo $full_name; ?>">
        </div>
        <div class="mb-3">
            <label for="date-of-birth" class="form-label">Date of Birth</label>
            <input type="date" data-name="Date of Birth" class="form-control required" id="date-of-birth" name="date_of_birth" placeholder="Enter Date of Birth" value="<?php echo $date_of_birth; ?>">
        </div>
        <div class="mb-3">
            <label for="gender" class="form-label">Gender</label>
            <input type="text" data-name="Gender" class="form-control required" id="gender" name="gender" placeholder="Enter Gender" value="<?php echo $gender; ?>">
        </div>
        <div class="mb-3">
            <label for="ethnicity" class="form-label">Ethnicity</label>
            <input type="text" data-name="Ethnicity" class="form-control required" id="ethnicity" name="ethnicity" placeholder="Enter Ethnicity" value="<?php echo $ethnicity; ?>">
        </div>
        <div class="mb-3">
            <label for="height" class="form-label">Height</label>
            <input type="text" data-name="Height" class="form-control required" id="height" name="height" placeholder="Enter Height in (CM)" value="<?php echo $height; ?>">
        </div>
        <div class="mb-3">
            <label for="weight" class="form-label">Weight</label>
            <input type="text" data-name="Weight" class="form-control required" id="weight" name="weight" placeholder="Enter Weight in (KG)" value="<?php echo $weight; ?>">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block edit-profile-btn">EDIT PROFILE</button>
    </form>
</div>