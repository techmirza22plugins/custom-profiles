<div class="custom-profiles-change-password-shortcode">
    <h3>CHANGE PASSWORD</h3>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Success!</strong> Your password has been changed successfully.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="change-password-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_change_password">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" data-name="Password" class="form-control required" id="password" name="password" placeholder="Enter Password">
        </div>
        <div class="mb-3">
            <label for="retype-password" class="form-label">Retype Password</label>
            <input type="password" data-name="Retype Password" class="form-control required" id="retype-password" name="retype_password" placeholder="Retype Password">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block change-password-btn">CHANGE PASSWORD</button>
    </form>
</div>