<?php
    $customProfilesSettings = get_option('custom_profiles_settings', array());
    $custom_profiles_change_password_page_id = $customProfilesSettings['custom_profiles_change_password_page_id'];
    $custom_profiles_manage_users_page_id = $customProfilesSettings['custom_profiles_manage_users_page_id'];
    $custom_profiles_support_page_id = $customProfilesSettings['custom_profiles_support_page_id'];
    $custom_profiles_terms_of_use_page_id = $customProfilesSettings['custom_profiles_terms_of_use_page_id'];
?>
<div class="custom-profiles-settings-shortcode mx-auto">
    <h3 class="text-center">SETTINGS</h3>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-8 pl-4">
                    CHANGE PASSWORD
                </div>
                <div class="col-4 text-right pr-4">
                    <a href="<?php echo get_permalink((int) $custom_profiles_change_password_page_id); ?>"><i class="fa fa-lg fa-arrow-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-8 pl-4">
                    MANAGE USERS
                </div>
                <div class="col-4 text-right pr-4">
                    <a href="<?php echo get_permalink((int) $custom_profiles_manage_users_page_id); ?>"><i class="fa fa-lg fa-arrow-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-8 pl-4">
                    SUPPORT
                </div>
                <div class="col-4 text-right pr-4">
                    <a href="<?php echo get_permalink((int) $custom_profiles_support_page_id); ?>"><i class="fa fa-lg fa-arrow-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-8 pl-4">
                    TERMS OF USE
                </div>
                <div class="col-4 text-right pr-4">
                    <a href="<?php echo get_permalink((int) $custom_profiles_terms_of_use_page_id); ?>"><i class="fa fa-lg fa-arrow-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>