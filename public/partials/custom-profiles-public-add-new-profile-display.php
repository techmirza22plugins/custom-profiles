<div class="custom-profiles-add-new-profile-shortcode">
    <h3>ADD NEW PROFILE</h3>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="add-new-profile-form" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="action" value="custom_profiles_add_new_profile">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Profile added successfully!</strong> Redirecting...
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['upload-message']) && !empty($_GET['upload-message'])) : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Error!</strong> <?php echo $_GET['upload-message']; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <div class="custom-profiles-profile-picture profile-picture mx-auto mb-3">
            <div class="circle">
                <img class="profile-pic" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU">
            </div>
            <div class="p-image">
                <button type="button" class="btn btn-dark btn-circle btn-sm btn-upload-circle"><i class="fa fa-camera upload-button mr-0"></i></button>
                <input class="file-upload form-control" name="profile_picture" type="file" accept="image/*"/>
            </div>
        </div>
        <div class="mb-3">
            <label for="barcode" class="form-label">Barcode</label>
            <input type="text" data-name="Barcode" class="form-control required" id="barcode" name="barcode" placeholder="Enter Barcode From Your Box">
        </div>
        <div class="mb-3">
            <label for="full-name" class="form-label">Full Name</label>
            <input type="text" data-name="Full Name" class="form-control required" id="full-name" name="full_name" placeholder="Enter Full Name">
        </div>
        <div class="mb-3">
            <label for="date-of-birth" class="form-label">Date of Birth</label>
            <input type="date" data-name="Date of Birth" class="form-control required" id="date-of-birth" name="date_of_birth" placeholder="Enter Date of Birth">
        </div>
        <div class="mb-3">
            <label for="gender" class="form-label">Gender</label>
            <input type="text" data-name="Gender" class="form-control required" id="gender" name="gender" placeholder="Enter Gender">
        </div>
        <div class="mb-3">
            <label for="ethnicity" class="form-label">Ethnicity</label>
            <input type="text" data-name="Ethnicity" class="form-control required" id="ethnicity" name="ethnicity" placeholder="Enter Ethnicity">
        </div>
        <div class="mb-3">
            <label for="height" class="form-label">Height</label>
            <input type="text" data-name="Height" class="form-control required" id="height" name="height" placeholder="Enter Height in (CM)">
        </div>
        <div class="mb-3">
            <label for="weight" class="form-label">Weight</label>
            <input type="text" data-name="Weight" class="form-control required" id="weight" name="weight" placeholder="Enter Weight in (KG)">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block add-new-profile-btn">ADD NEW PROFILE</button>
    </form>
</div>