<?php
    $customProfilesSettings = get_option('custom_profiles_settings', array());
    $custom_profiles_forgot_password_page_id = $customProfilesSettings['custom_profiles_forgot_password_page_id'];
?>
<div class="custom-profiles-login-shortcode">
    <h3>LOGIN TO YOUR ACCOUNT</h3>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Wrong Email / Username and Password!</strong> Please try again.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($_GET['activated']) && !empty($_GET['activated']) && $_GET['activated'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert" style="display: block;">
            <strong>Your account is not activated.</strong> Please contact admin.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($_GET['change-password']) && !empty($_GET['change-password']) && $_GET['change-password'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Password has been changed!</strong> Please login with new password.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($_GET['welcome']) && !empty($_GET['welcome']) && $_GET['welcome'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert" style="display: block;">
            <strong>Thanks for registration!</strong> Please login to your account.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <form action="<?php echo admin_url('admin-post.php'); ?>" class="login-form" method="POST">
        <input type="hidden" name="action" value="custom_profiles_login">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" data-name="Email" class="form-control required" id="email" name="email" placeholder="Enter Email">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" data-name="Password" class="form-control required" id="password" name="password" placeholder="Enter Password">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
        <p class="text-center mb-0 mt-3">Forgot Password? <a href="<?php echo get_permalink($custom_profiles_forgot_password_page_id); ?>">Click Here</a></p>
    </form>
</div>