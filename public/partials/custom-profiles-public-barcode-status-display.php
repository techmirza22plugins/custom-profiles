<?php
    $barcode_status = get_post_meta( $profile_id, 'barcode_status', true );
?>
<div class="custom-profiles-barcode-status-shortcode">
    <?php if ($barcode_status == 'inactive') { ?>
        <h3><span class="badge bg-danger">Not Set</span></h3>
    <?php } else { ?>
        <h3><span class="badge bg-success"><?php echo $barcode_status; ?></span></h3>
    <?php } ?>
</div>