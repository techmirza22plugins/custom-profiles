(function( $ ) {

	function validateEmail(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;
	
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');
	
			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
	};

	
	var readURL = function(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(document).find(".custom-profiles-profile-picture .profile-pic").attr("src", e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	};
	
	$(document).ready(function() {
		// DataTables Initilization
		$(document).find(".custom-profiles-barcode-dashbaord-shortcode .datatable").dataTable();
		// Registration Form Validation
		$(document).on("click", ".register-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
				if ($(this).attr("type") == "email" && $(this).val()) {
					if (!validateEmail($(this).val())) {
						currentForm.find(".alert-danger").html("Email doesn't seems to be valid. Please try again!");
						validation = false;
						return false;
					} 
				}
				if ($(this).attr("type") == "password" && $(this).attr("id") == "retype-password" && $(this).val()) {
					if ($(this).val() != currentForm.find("#password").val()) {
						currentForm.find(".alert-danger").html("Password doesn't seems to be similar. Please try again!");
						validation = false;
						return false;
					}
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				var data = {
					'action': 'check_email_address',
					'email': currentForm.find("#email").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						currentForm.find(".alert-danger").html("This email address is already in use. Please find another!").show();
						return false;
					} else {
						currentForm.submit();
					}
				});
			}
		});
		var register = getUrlParameter("register");
		if (register == "true") {
			setTimeout(function() {
				var user = getUrlParameter("user");
				location.href = my_ajax_object.activate_barcode_page_url + "?user=" + user;
			}, 3000);
		}
		// Registration Form Validation
		// Login Form Validation
		$(document).on("click", ".login-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
				if ($(this).attr("type") == "email" && $(this).val()) {
					if (!validateEmail($(this).val())) {
						currentForm.find(".alert-danger").html("Email doesn't seems to be valid. Please try again!");
						validation = false;
						return false;
					} 
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				currentForm.submit();
			}
		});
		// Login Form Validation
		// Activate Kit Form Validation
		$(document).on("click", ".activate-kit-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				var data = {
					'action': 'check_barcode',
					'barcode': currentForm.find("#barcode").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if ( response == "NotFound" ) {
						currentForm.find(".alert-danger").html("Sorry! Your entered barcode seems not to be correct.").show();
						return false;
					} else if ( response == "Used" ) {
						currentForm.find(".alert-danger").html("Sorry! Your entered barcode already have been used.").show();
						return false;
					} else {
						currentForm.submit();
					}
				});
			}
		});
		// Activate Kit Form Validation
		// Change Password Form Validation
		$(document).on("click", ".change-password-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
				if ($(this).attr("type") == "password" && $(this).attr("id") == "retype-password" && $(this).val()) {
					if ($(this).val() != currentForm.find("#password").val()) {
						currentForm.find(".alert-danger").html("Password doesn't seems to be similar. Please try again!");
						validation = false;
						return false;
					}
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				currentForm.submit();
			}
		});
		// Change Password Form Validation
		// Add New Profile Form Validation
		$(document).on("click", ".add-new-profile-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).attr("type") == "file" && $(this).get(0).files.length === 0) {
					currentForm.find(".alert-danger").html("Profile Picture is required field.");
					validation = false;
					return false;
				} else {
					if ($(this).hasClass("required") && ! $(this).val()) {
						currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
						validation = false;
						return false;
					}
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				$('html, body').animate({
					scrollTop: $(document).find("body").offset().top
				}, 2000);
				return false;
			} else {
				var data = {
					'action': 'check_barcode',
					'barcode': currentForm.find("#barcode").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if ( response == "NotFound" ) {
						currentForm.find(".alert-danger").html("Sorry! Your entered barcode seems not to be correct.").show();
						$('html, body').animate({
							scrollTop: $(document).find("body").offset().top
						}, 2000);
						return false;
					} else if ( response == "Used" ) {
						currentForm.find(".alert-danger").html("Sorry! Your entered barcode already have been used.").show();
						$('html, body').animate({
							scrollTop: $(document).find("body").offset().top
						}, 2000);
						return false;
					} else {
						currentForm.submit();
					}
				});
			}
		});
		// Add New Profile Form Validation
		// Edit Profile Form Validation
		$(document).on("click", ".edit-profile-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).attr("type") == "file" && $(this).get(0).files.length === 0) {
					if ($(document).find(".profile-pic").attr("src") == "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-XdxI4OtQD4UMsyCoV5U5TeyZDf3jcXYPog&usqp=CAU") {
						currentForm.find(".alert-danger").html("Profile Picture is required field.");
						validation = false;
						return false;
					} else {
						//
					}
				} else {
					if ($(this).hasClass("required") && ! $(this).val()) {
						currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
						validation = false;
						return false;
					}
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				$('html, body').animate({
					scrollTop: $(document).find("body").offset().top
				}, 2000);
				return false;
			} else {
				currentForm.submit();
			}
		});
		// Edit Profile Form Validation
		// Upload Button for Profile Change
		$(document).on("click", ".custom-profiles-profile-picture .btn-upload-circle", function() {
			$(this).next().trigger("click");
		});
		$(document).on("change", ".custom-profiles-profile-picture .file-upload", function() {
			readURL(this);
		});
		$(document).on("change", ".custom-profiles-profile-picture .upload-button", function() {
		   $(document).find(".custom-profiles-profile-picture .file-upload").click();
		});
		// Profile Picture Credentials
		$(document).find(".profile-dropdown-menu li").each(function() {
			if ($(this).find("a.active-user").length > 0) {
				$(document).find(".profile-switch-btn").text($(this).find("a.active-user").text());
				$(document).find(".nav-item-profile-picture").attr("src", $(this).find("a.active-user").attr("data-profile-picture"));
			}
		})
		// Forgot Password Form Validation
		$(document).on("click", ".forgot-password-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				var data = {
					'action': 'check_email_address',
					'email': currentForm.find("#email").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						currentForm.submit();
					} else {
						currentForm.find(".alert-danger").html("Sorry! This email doesn't exist. Please try again.").show();
						return false;
					}
				});
			}
		});
		// Forgot Password Form Validation

		// Reset Password Form Validation
		$(document).on("click", ".reset-password-btn", function() {
			var currentForm = $(this).closest("form");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
				if ($(this).attr("type") == "password" && $(this).attr("id") == "retype-password" && $(this).val()) {
					if ($(this).val() != currentForm.find("#password").val()) {
						currentForm.find(".alert-danger").html("Password doesn't seems to be similar. Please try again!");
						validation = false;
						return false;
					}
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				currentForm.submit();
			}
		});
		// Reset Password Form Validation

		// Modal Open Button
		$(document).on("click", ".modal-open-btn", function() {
			var currentTr = $(this).closest("tr");
			var profileID = currentTr.data("profile-id");
			var barcode = currentTr.find("td:eq(1)").text();
			var barcodeStatus = currentTr.find("td:eq(2) span").data("barcode-status");
			var currentForm = $(document).find("form.update-status");
			currentForm.find("input[name=profile_id]").val(profileID);
			currentForm.find("#barcode").val(barcode);
			if (barcodeStatus) {
				currentForm.find("#status").val(barcodeStatus);
			} else {
				currentForm.find("#status").val("");
			}
		});
		// Modal Open Button

		// Barcode Status Update Form Validation
		$(document).on("click", ".barcode-status-update-btn", function() {
			var currentForm = $(document).find("form.update-status");
			var validation = true;
			currentForm.find(".form-control").each(function() {
				if ($(this).hasClass("required") && ! $(this).val()) {
					currentForm.find(".alert-danger").html($(this).data("name") + " is required field.");
					validation = false;
					return false;
				}
			});
			if (!validation) {
				currentForm.find(".alert-danger").show();
				return false;
			} else {
				currentForm.submit();
			}
		});
		// Barcode Status Update Form Validation
	});

})( jQuery );
