<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Profiles {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Custom_Profiles_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CUSTOM_PROFILES_VERSION' ) ) {
			$this->version = CUSTOM_PROFILES_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'custom-profiles';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Custom_Profiles_Loader. Orchestrates the hooks of the plugin.
	 * - Custom_Profiles_i18n. Defines internationalization functionality.
	 * - Custom_Profiles_Admin. Defines all hooks for the admin area.
	 * - Custom_Profiles_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-profiles-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-profiles-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-custom-profiles-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-custom-profiles-public.php';

		$this->loader = new Custom_Profiles_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Custom_Profiles_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Custom_Profiles_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Custom_Profiles_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'custom_profiles_plugin_settings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'custom_profiles_plugin_menu' );

		$this->loader->add_action( 'admin_post_custom_profiles_register', $plugin_admin, 'custom_profiles_register' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_register', $plugin_admin, 'custom_profiles_register' );

		$this->loader->add_action( 'admin_post_custom_profiles_activate_kit', $plugin_admin, 'custom_profiles_activate_kit' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_activate_kit', $plugin_admin, 'custom_profiles_activate_kit' );

		$this->loader->add_action( 'admin_post_custom_profiles_login', $plugin_admin, 'custom_profiles_login' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_login', $plugin_admin, 'custom_profiles_login' );

		$this->loader->add_action( 'admin_post_custom_profiles_change_password', $plugin_admin, 'custom_profiles_change_password' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_change_password', $plugin_admin, 'custom_profiles_change_password' );

		$this->loader->add_action( 'admin_post_custom_profiles_forgot_password', $plugin_admin, 'custom_profiles_forgot_password' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_forgot_password', $plugin_admin, 'custom_profiles_forgot_password' );

		$this->loader->add_action( 'admin_post_custom_profiles_reset_password', $plugin_admin, 'custom_profiles_reset_password' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_reset_password', $plugin_admin, 'custom_profiles_reset_password' );

		$this->loader->add_action( 'admin_post_custom_profiles_add_new_profile', $plugin_admin, 'custom_profiles_add_new_profile' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_add_new_profile', $plugin_admin, 'custom_profiles_add_new_profile' );

		$this->loader->add_action( 'admin_post_custom_profiles_edit_profile', $plugin_admin, 'custom_profiles_edit_profile' );
		$this->loader->add_action( 'admin_post_nopriv_custom_profiles_edit_profile', $plugin_admin, 'custom_profiles_edit_profile' );

		$this->loader->add_action( 'admin_post_update_barcode_status', $plugin_admin, 'update_barcode_status' );
		$this->loader->add_action( 'admin_post_nopriv_update_barcode_status', $plugin_admin, 'update_barcode_status' );

		$this->loader->add_action( 'wp_ajax_check_email_address', $plugin_admin, 'check_email_address' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_email_address', $plugin_admin, 'check_email_address' );

		$this->loader->add_filter( 'manage_users_columns', $plugin_admin, 'new_modify_user_table' );
		$this->loader->add_filter( 'manage_users_custom_column', $plugin_admin, 'new_modify_user_table_row', 10, 3 );

		$this->loader->add_action( 'admin_footer', $plugin_admin, 'lh_add_livechat_js_code' );

		$this->loader->add_action( 'wp_ajax_get_user_profiles', $plugin_admin, 'get_user_profiles' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_user_profiles', $plugin_admin, 'get_user_profiles' );

		$this->loader->add_action( 'wp_ajax_check_barcode', $plugin_admin, 'check_barcode' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_barcode', $plugin_admin, 'check_barcode' );

		$this->loader->add_action( 'wp_ajax_count_file_contents', $plugin_admin, 'count_file_contents' );

		$this->loader->add_action( 'wp_ajax_import_barcodes_records', $plugin_admin, 'import_barcodes_records' );

		$this->loader->add_action( 'wp_mail_content_type', $plugin_admin, 'wpse27856_set_content_type' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Custom_Profiles_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_filter( 'wp_nav_menu_items', $plugin_public, 'new_nav_menu_items' );

		$plugin_public->add_shortcodes();

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Custom_Profiles_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
