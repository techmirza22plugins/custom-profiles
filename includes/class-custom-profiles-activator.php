<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Custom_Profiles
 * @subpackage Custom_Profiles/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Profiles_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		add_role( 'lab-manager', 'Lab Manager', array(
			'read' => true, // True allows that capability
		));
	}

}
